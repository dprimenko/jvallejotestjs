
// Si agrego un formulario más, podría validarlo con esta función?
function validarForm(){

    // Uso de variables antes que constantes
    var error = 0;

    // Uso de array antes que un objecto
    var message = [
        '<p class="success">El formulario se ha rellenada correctamente.</p>',
        '<p class="error">El formulario no cumple alguna de las restricciones.</p>'
    ];

    // Acceso global a selectores
    var nombre = $('input[name=nombre]').val();
    var apellido = $('input[name=apellido]').val();
    var telefono = $('input[name=telefono]').val();
    var email = $('input[name=email]').val();


    // Duplicidad de código para la acción de validar
    // Uso de número mágicos
    if( nombre == '' || nombre.length > 10){
        error = 1;
    }

    // Difencia entre == y ===
    if( apellido == '' || apellido.length > 20){
        error = 1;
    }

    if( telefono.length > 9 || isNaN(telefono)){
        error = 1;
    }

    // Si tuvieramos ahora que validar que el dominio del email contiene el formato ###.###, como podriamos hacerlo?
    if( email == '' || email.search('@') < 0){
        error = 1;
    }

    $('.message').html(message[error]);

    // Uso de return false
    return false;
};
